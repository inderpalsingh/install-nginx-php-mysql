#!/bin/bash

clear

if [[ `id -u` != 0 ]]; then
    echo >&2 "Must be root to run script";
    exit;
fi


read -p "DOMANIN_NAME: " DOMAIN_NAME;
read -p "MYSQL_ROOT_PASSWORD: " MYSQL_ROOT_PASSWORD;
read -p "PHP_VERSION: " PHP_VERSION;
read -p "PHPMYADMIN_VER: " PHPMYADMIN_VER;


if [[ $DOMAIN_NAME == "" ]]; then
    echo >&2 "DOMAIN_NAME is empty";
    exit;
fi
if [[ $MYSQL_ROOT_PASSWORD == "" ]]; then
    echo >&2 "MYSQL_ROOT_PASSWORD is empty";
    exit;
fi
if [[ $PHP_VERSION == "" ]]; then
    echo >&2 "MYSQL_ROOT_PASSWORD is empty";
    exit;
fi

if [[ $PHPMYADMIN_VER == "" ]]; then
    echo >&2 "PHPMYADMIN_VER is empty";
    exit;
fi

### update repo
apt-get -y update
# mv /etc/apt/trusted.gpg /etc/apt/trusted.gpg.d/
add-apt-repository ppa:ondrej/php -y
apt-get -y install unzip debconf-utils software-properties-common zip unzip snapd tar curl wget
apt-get -y update

echo "-----------------------------------------------------"
echo "STEP #1 (Setup Mysql,Nginx and PHP $PHP_VERSION)"
echo "-----------------------------------------------------"

echo "mysql-server mysql-server/root_password password $MYSQL_ROOT_PASSWORD" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $MYSQL_ROOT_PASSWORD" | debconf-set-selections

# Make Maria connectable from outside world without SSH tunnel
if [ $2 == "true" ]; then
    # enable remote access
    # setting the mysql bind-address to allow connections from everywhere
    sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/my.cnf

    # adding grant privileges to mysql root user from everywhere
    # thx to http://stackoverflow.com/questions/7528967/how-to-grant-mysql-privileges-in-a-bash-script for this
    MYSQL=`which mysql`

    Q1="GRANT ALL ON *.* TO 'root'@'%' IDENTIFIED BY '$1' WITH GRANT OPTION;"
    Q2="FLUSH PRIVILEGES;"
    SQL="${Q1}${Q2}"

    $MYSQL -uroot -p$1 -e "$SQL"

    systemctl restart mysql.service
fi

sleep 10

rm /var/lib/dpkg/lock
rm /var/lib/dpkg/lock-frontend
rm /var/lib/apt/lists/lock
rm /var/cache/apt/archives/lock



apt-get -y install php$PHP_VERSION-fpm php$PHP_VERSION-common php$PHP_VERSION-mysql \
    php$PHP_VERSION-xml php$PHP_VERSION-xmlrpc php$PHP_VERSION-curl \
    php$PHP_VERSION-gd php$PHP_VERSION-imagick php$PHP_VERSION-cli php$PHP_VERSION-dev \
    php$PHP_VERSION-imap php$PHP_VERSION-mbstring php$PHP_VERSION-soap php$PHP_VERSION-zip php$PHP_VERSION-bcmath php$PHP_VERSION-redis

sleep 10
rm /var/lib/dpkg/lock
rm /var/lib/dpkg/lock-frontend
rm /var/lib/apt/lists/lock
rm /var/cache/apt/archives/lock

echo "-----------------------------------------------------"
echo "STEP #2 (Downloading and setup phpmyadmin)"
echo "-----------------------------------------------------"
cd /usr/share/
wget https://files.phpmyadmin.net/phpMyAdmin/$PHPMYADMIN_VER/phpMyAdmin-$PHPMYADMIN_VER-english.zip .
unzip phpMyAdmin-$PHPMYADMIN_VER-english.zip && mv phpMyAdmin-$PHPMYADMIN_VER-english phpmyadmin
chown -R www-data:www-data /usr/share/phpmyadmin
rm phpMyAdmin-$PHPMYADMIN_VER-english.zip

sleep 10


echo "-----------------------------------------------------"
echo "STEP #3 (Installing VScode)"
echo "-----------------------------------------------------"
snap install code --classic

rm /var/lib/dpkg/lock
rm /var/lib/dpkg/lock-frontend
rm /var/lib/apt/lists/lock
rm /var/cache/apt/archives/lock

sleep 5
apt-get -y install nginx mysql-server

echo "-----------------------------------------------------"
echo "STEP #3 (Installing Redis and Domain setup)"
echo "-----------------------------------------------------"
apt install redis-server -y

systemctl restart php$PHP_VERSION-fpm  && systemctl restart nginx


mkdir /var/www/html/$DOMAIN_NAME/;
chown -R $USER:www-data /var/www/html/$DOMAIN_NAME/;
chmod -R 2775 /var/www/html/$DOMAIN_NAME/;
cp /etc/nginx/sites-available/default /etc/nginx/sites-available/default_org


sleep 5


cat << EOF > /etc/nginx/sites-available/$DOMAIN_NAME.conf

    server {
        listen 80;

        server_name $DOMAIN_NAME;
        root /var/www/html/$DOMAIN_NAME/;
        index  index.php;
        # log files
        access_log /var/log/nginx/$DOMAIN_NAME.access.log;
        error_log /var/log/nginx/$DOMAIN_NAME.error.log;
        location / {
            try_files \$uri \$uri/ /index.php?\$args;
        }
        location ~ \.php$ {
            include snippets/fastcgi-php.conf;
            fastcgi_pass unix:/run/php/php$PHP_VERSION-fpm.sock;
        }
    }
EOF
sleep 5


ln -s /etc/nginx/sites-available/$DOMAIN_NAME.conf /etc/nginx/sites-enabled/
systemctl reload php$PHP_VERSION-fpm  && systemctl reload nginx

echo "127.0.0.1  $DOMAIN_NAME" >> /etc/hosts
echo "-----------------------------------------------------"
echo "STEP #4 (System going to reboot)"
echo "-----------------------------------------------------"


sleep 5
init 6
